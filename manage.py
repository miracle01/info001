from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from info import db, create_app, models

# 创建 app，并传入配置模式：development / production / testing
app = create_app("development")

manager = Manager(app)
# 数据库迁移
Migrate(app, db)
manager.add_command("db", MigrateCommand)



if __name__ == '__main__':
    manager.run()
