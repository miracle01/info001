import redis
import logging


class BaseConfig(object):
    """工程配置信息"""
    DEBUG = True

    SECRET_KEY = "EjpNVSNQTyGi1VvWECj9TvC/+kq3oujee2kTfQUs8yCM6xX9Yjq52v54g+HVoknA"

    # 数据库的配置信息
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/info001"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # redis配置
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379

    # flask_session的配置信息
    SESSION_TYPE = "redis"  # 指定 session 保存到 redis 中
    SESSION_USE_SIGNER = True  # 让 cookie 中的 session_id 被加密签名处理
    SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)  # 使用 redis 的实例
    PERMANENT_SESSION_LIFETIME = 60 * 60 * 60 * 24 * 2  # session 的有效期，单位是秒
    # 默认日志等级
    LOG_LEVEL = logging.DEBUG

class DevelopmentConfig(BaseConfig):
    """开发模式下的配置"""
    DEBUG = True


class ProductionConfig(BaseConfig):
    """生产模式下的配置"""
    DEBUG = False
    LOG_LEVEL = logging.ERROR


class TestingConfig(BaseConfig):
    """单元测试环境下的配置"""
    DEBUG = True
    TESTING = True


config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "testing": TestingConfig
}
